
function TestAnimation3(resources)
{
	TestAnimation3.resources = resources;
}
TestAnimation3.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(900, 470, Phaser.CANVAS, 'TestAnimation3', { preload: this.preload, create: this.create, update: this.update, render:
		this.render,parent:this});
	},

	preload: function()
	{
		this.game.scale.maxWidth = 900;
    	this.game.scale.maxHeight = 500;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

		this.game.load.image('farleftImage', TestAnimation3.resources.farleftImage);
    	this.game.load.image('leftImage', TestAnimation3.resources.leftImage);
    	this.game.load.image('centerImage', TestAnimation3.resources.centerImage);
    	this.game.load.image('rightImage', TestAnimation3.resources.rightImage);

    	this.game.stage.backgroundColor = '#ffffff';

	},

	create: function(evt)
	{
		this.game.stage.backgroundColor = '#ffffff'

	    this.parent.farleftImage = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'farleftImage');
		this.parent.farleftImage.alpha = 1;
		this.parent.farleftImage.smoothed = false;

		this.parent.leftImage = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'leftImage');
		this.parent.leftImage.alpha = 1;
		this.parent.leftImage.smoothed = false;

		this.parent.centerImage = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'centerImage');
		this.parent.centerImage.alpha = 1;
		this.parent.centerImage.smoothed = false;

		this.parent.rightImage = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'rightImage');
		this.parent.rightImage.alpha = 1;
		this.parent.rightImage.smoothed = false;

		var style = { font: "19px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth:150, align: "center",lineSpacing: -10 };
		var style2 = { font: "bold 25px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth:150, align: "center",lineSpacing: -10 };


		this.parent.farleftText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, TestAnimation3.resources.farleftText, style);
		this.parent.leftText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, TestAnimation3.resources.leftText, style);
		this.parent.centerText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, TestAnimation3.resources.centerText, style);
		this.parent.rightText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, TestAnimation3.resources.rightText, style);
		this.parent.buildAnimation();


		this.parent.farleftText.smoothed = false;
		this.parent.leftText.smoothed = false;
		this.parent.centerText.smoothed = false;
		this.parent.rightText.smoothed = false;

	},

	buildAnimation: function()
	{

		this.rightImage.anchor.set(0.5);
		this.centerImage.anchor.set(0.5);
		this.farleftImage.anchor.set(0.5);
		this.leftImage.anchor.set(0.5);

		this.farleftText.anchor.set(0.5);
		this.leftText.anchor.set(0.5);
		this.centerText.anchor.set(0.5);
		this.rightText.anchor.set(0.5);
		//
	    this.farleftText.alpha = 0;
		this.farleftText.x = this.game.world.centerX-300;
		this.farleftText.y = this.game.world.centerY+70;
		this.farleftText.originY = this.farleftText.y;

		this.leftText.alpha = 0;
		this.leftText.x = this.game.world.centerX-100;
		this.leftText.y = this.game.world.centerY+70;
		this.leftText.originY = this.leftText.y;

		this.centerText.alpha = 0;
		this.centerText.x = this.game.world.centerX+100;
		this.centerText.y = this.game.world.centerY+70;
		this.centerText.originY = this.centerText.y;

		this.rightText.x = this.game.world.centerX+300;
		this.rightText.y = this.game.world.centerY+70;
		this.rightText.originY = this.rightText.y;
		this.rightText.alpha = 0;


		this.farleftImage.x = this.game.world.centerX-300;
		this.farleftImage.y = this.game.world.centerY-160;
		this.farleftImage.originY = this.farleftImage.y;
		this.farleftImage.alpha = 0;

		this.leftImage.x = this.game.world.centerX-100;
		this.leftImage.y = this.game.world.centerY-160;
		this.leftImage.originY = this.leftImage.y;
		this.leftImage.alpha = 0;

		this.centerImage.y = this.game.world.centerY-160;
		this.centerImage.x = this.game.world.centerX+100;
		this.centerImage.originY = this.centerImage.y;
		this.centerImage.alpha = 0;

		this.rightImage.y = this.game.world.centerY-160;
		this.rightImage.x = this.game.world.centerX+300;
		this.rightImage.originY = this.rightImage.y;
		this.rightImage.alpha = 0;

		//
	    this.farleftImage.y-=150;
		this.farleftImageAn = this.game.add.tween(this.farleftImage).to( { alpha:1,y: this.farleftImage.originY +120},800,Phaser.Easing.Quadratic.Out);
		//
		this.farleftText.y+=150;
		this.farleftTextAn = this.game.add.tween(this.farleftText).to( { alpha:1,y:this.farleftText.originY +120},800,Phaser.Easing.Quadratic.Out);
		//

		this.leftImage.y-=150;
		this.leftImageAn = this.game.add.tween(this.leftImage).to( { alpha:1,y: this.leftImage.originY +120},800,Phaser.Easing.Quadratic.Out);
		//
		this.leftText.y+=150;
		this.leftTextAn = this.game.add.tween(this.leftText).to( { alpha:1,y:this.leftText.originY +120},800,Phaser.Easing.Quadratic.Out);
		//
		this.centerImage.y-=150;
		this.centerImageAn = this.game.add.tween(this.centerImage).to( { alpha:1,y:this.centerImage.originY+120},800,Phaser.Easing.Quadratic.Out);
		//
		this.centerText.y+=150;
		this.centerTextAn = this.game.add.tween(this.centerText).to( { alpha:1,y:this.centerText.originY +120},800,Phaser.Easing.Quadratic.Out);

		this.rightImage.y-=150
		this.rightImageAn = this.game.add.tween(this.rightImage).to( { alpha:1,y: this.rightImage.originY +120},800,Phaser.Easing.Quadratic.Out);

		this.rightText.y+=150;
		this.rightTextAn = this.game.add.tween(this.rightText).to( {alpha:1,y:this.rightText.originY +120},800,Phaser.Easing.Quadratic.Out);


		this.farleftImageAn.chain (this.farleftTextAn, this.leftImageAn, this.leftTextAn, this.centerImageAn,this.centerTextAn,this.rightImageAn,this.rightTextAn);
		this.farleftImageAn.start();


	},

	update: function()
	{

	},

	render: function()
	{
		 //this.game.debug.inputInfo(32, 32);
	}

}
